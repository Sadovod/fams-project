# Установка #

1. Установить Python 3.8.1
2. Выполнить `git clone https://Sadovod@bitbucket.org/Sadovod/autosport.git` в нужной директории;
3. Создать виртуальное окружение в данной директории: `python -m venv название_окружения`;
4. Активировать виртуальное окружение.;
    - Для Linux: `source название_окружения/bin/activate`
    - Для Windows: `название_окружения\Scripts\activate.bat`
5. Установить зависимости: `pip install -r requirements.txt`;
6. Войти в папку website и ввести в терминале (запуск сервера): `python manage.py runserver`;
