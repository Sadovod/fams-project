from django.urls import path
from .views import signup, confirmation_form_view, login_view, logout_view, forgot_password, reset_password

urlpatterns = [
    path('signup/', signup, name='signup'),
    path('confirmation_form/', confirmation_form_view, name='confirmation_form'),
    path('signin/', login_view, name='signin'),
    path('signout/', logout_view, name='signout'),
    path('forgot_password/', forgot_password, name='forgot_password'),
    path('reset_password/', reset_password, name='reset_password'),
    # path('profile/', 1, name='profile'),
    # path('edit_profile/', 1, name='edit_profile'),
    # path('change_password/', 1, name='change_password'),
]
