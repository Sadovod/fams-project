from django.apps import AppConfig


class AccountsConfig(AppConfig):
    name = 'apps_dir.accounts'
