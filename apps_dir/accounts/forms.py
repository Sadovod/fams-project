from django import forms
from .models import CustomUser
from django.contrib.auth.forms import UserCreationForm, SetPasswordForm


class CustomUserForm(UserCreationForm):
    phone = forms.CharField(max_length=11)

    class Meta:
        model = CustomUser
        fields = ['username', 'first_name', 'last_name', 'phone', 'password1', 'password2']

    def clean_phone(self, *args, **kwargs):
        phone = self.cleaned_data.get('phone')
        if CustomUser.objects.filter(phone=phone, is_verified=True).exists():
            raise forms.ValidationError('Этот номер телефона уже занят')
        else:
            return phone


class ConfirmationForm(forms.Form):
    code = forms.CharField(max_length=5)


class ForgotPasswordForm(forms.Form):
    username = forms.CharField(max_length=50)

    def clean_username(self, *args, **kwargs):
        username = self.cleaned_data['username']
        if CustomUser.objects.filter(username=username).exists():
            return username
        else:
            raise forms.ValidationError('Пользователя с таким логином не существует')


class CustomResetPasswordForm(SetPasswordForm):
    code = forms.CharField(max_length=5)

    # class Meta:
    #     fields = ['code', 'new_password1', 'new_password2']
