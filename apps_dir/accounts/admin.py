from django.contrib import admin
from .models import CustomUser


class RaceInline(admin.TabularInline):
    model = CustomUser.races.through
    extra = 1
    verbose_name = 'Гонка'
    verbose_name_plural = 'Гонки'


@admin.register(CustomUser)
class CustomUserAdmin(admin.ModelAdmin):
    list_display = ['username', 'phone', 'is_verified']
    readonly_fields = ['username']
    exclude = ['races']
    list_filter = ('is_verified',)
    inlines = [RaceInline]
