from django.contrib.auth import logout, login, authenticate
from django.contrib import messages
from django.db import transaction
from django.contrib.auth.forms import AuthenticationForm
from django.shortcuts import render, redirect, reverse
from .forms import CustomUserForm, ConfirmationForm, ForgotPasswordForm, CustomResetPasswordForm
from .models import CustomUser
from .tasks import send_sms_code
from random import randint


@transaction.atomic()
def signup(request):
    return render(request, 'accounts/registration_is_closed.html')  # На то время пока регистрация закрыта

    # if not request.user.is_authenticated:
    #     if request.method == 'POST':
    #         form = CustomUserForm(request.POST)
    #         if form.is_valid():
    #             form.save()
    #             phone = form.cleaned_data['phone']
    #             conf_code = str(randint(10000, 99999))  # Код подтверждения регистрации
    #             request.session['true_conf_code'] = conf_code
    #             request.session['username_for_conf'] = form.cleaned_data['username']
    #             request.session['phone'] = form.cleaned_data['phone']
    #             send_sms_code.delay(phone, conf_code)  # Celery task для отправки смс с кодом
    #             return redirect(reverse('confirmation_form'))
    #         else:
    #             messages.error(request, 'Неверные данные')
    #             return render(request, 'accounts/signup.html', context={'forms': form})
    #
    #     elif request.method == 'GET':
    #         form = CustomUserForm()
    #         return render(request, 'accounts/signup.html', context={'forms': form})
    # else:
    #     messages.error(request, 'Вы уже зарегистрированны')
    #     return redirect('home')
 

@transaction.atomic()
def confirmation_form_view(request):
    if request.method == 'POST':
        form = ConfirmationForm(request.POST)
        if form.is_valid():

            phone = request.session.get('phone')
            true_conf_code = request.session.get('true_conf_code')
            code_from_form = form.cleaned_data['code']
            username = request.session.get('username_for_conf')

            if CustomUser.objects.select_for_update().filter(phone=phone, is_verified=True).exists():
                messages.error(request, 'Этот номер телефона уже занят')
                return redirect(reverse('signup'))
            else:
                if code_from_form == true_conf_code:
                    CustomUser.objects.filter(username=username).update(is_verified=True)
                    user = CustomUser.objects.get(username=username)
                    login(request, user)
                    messages.success(request, 'Вы успешно зарегестрированны')
                    return redirect(reverse('home'))
                else:
                    messages.error(request, 'Неверный код подтверждения!')
                    return redirect(reverse('confirmation_form'))

        else:
            return redirect(reverse('confirmation_form'))
    else:
        form = ConfirmationForm()
        return render(request, 'accounts/confirmation_form.html', context={'form': form})


def login_view(request):
    if request.method == 'POST':
        form = AuthenticationForm(request=request, data=request.POST)
        if form.is_valid():
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']
            user = authenticate(username=username, password=password)
            if user is not None:
                login(request, user)
                messages.success(request, 'Вход выполнен успешно')
                return redirect(reverse('home'))
            else:
                return redirect(reverse('signin'))
        else:
            return redirect(reverse('signin'))

    else:
        form = AuthenticationForm()
        return render(request, 'accounts/login.html', context={'forms': form})


def logout_view(request):
    logout(request)
    return redirect(reverse('home'))


def forgot_password(request):
    if request.method == 'POST':
        form = ForgotPasswordForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data['username']
            user_phone = CustomUser.objects.get(username=username).phone
            conf_code = str(randint(10000, 99999))
            request.session['username_for_reset_password'] = username
            request.session['code_for_reset_password'] = conf_code
            send_sms_code.delay(user_phone, conf_code)
            messages.success(request, 'На ваш телефон придет код подтверждения')
            return redirect(reverse('reset_password'))
        else:
            messages.error(request, 'Пользователя с таким логином не существует')
            return redirect(reverse('forgot_password'))

    else:
        form = ForgotPasswordForm()
        return render(request, 'accounts/forgot_password.html', context={'form': form})


@transaction.atomic()
def reset_password(request):
    username = request.session.get('username_for_reset_password')
    if CustomUser.objects.filter(username=username).exists():
        user = CustomUser.objects.get(username=username)
    else:
        messages.error(request, 'Пользователя с таким логином не существует')
        return redirect(reverse('home'))

    if request.method == 'POST':
        form = CustomResetPasswordForm(user, request.POST)
        if form.is_valid():
            conf_code = request.session.get('code_for_reset_password')
            code_from_form = form.cleaned_data['code']
            if code_from_form == conf_code:
                form.save()
                messages.success(request, 'Пароль успешно изменен')
                return redirect(reverse('home'))
            else:
                messages.error(request, 'Неверный код подтверждения')
                return redirect(reverse('reset_password'))
        else:
            messages.error(request, 'Неверные данные')
            return redirect(reverse('reset_password'))

    else:
        form = CustomResetPasswordForm(user)
        return render(request, 'accounts/reset_password.html', context={'form': form})
