from django.db import models
from django.contrib.auth.models import AbstractUser
from ..races.models import Race


class CustomUser(AbstractUser):
    first_name = models.CharField(max_length=25, verbose_name='Имя')
    last_name = models.CharField(max_length=25, verbose_name='Фамилия')
    phone = models.CharField(max_length=11, verbose_name='Телефон')
    races = models.ManyToManyField(Race, blank=True, verbose_name='Зарегестрирован на гонки')
    is_verified = models.BooleanField(verbose_name='Верифицирован', default=False)

    REQUIRED_FIELDS = ['phone', 'first_name', 'last_name']


# class BlockedUser(models.Model):
#     ip = models.IPAddressField(verbose_name='IP')
#     unblock_time = models.DateTimeField(verbose_name='Дата и время разблокировки')
#
#     class Meta:
#         verbose_name = 'Заблокированный адрес'
#         verbose_name_plural = 'Заблокированные адреса'

