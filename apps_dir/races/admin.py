from django.contrib import admin
from django import forms
from .models import RaceKind, Race, RaceKindDoc
from ckeditor_uploader.widgets import CKEditorUploadingWidget


@admin.register(RaceKind)
class KindAdmin(admin.ModelAdmin):
    list_display = ('name',)
    search_fields = ('name',)


class ResultsAdminForm(forms.ModelForm):
    results = forms.CharField(label='Таблица результатов', widget=CKEditorUploadingWidget())

    class Meta:
        model = Race
        fields = '__all__'


class RacersInline(admin.TabularInline):
    model = Race.customuser_set.through
    extra = 1
    verbose_name = 'Гонщик'
    verbose_name_plural = 'Гонщики'


@admin.register(Race)
class RaceAdmin(admin.ModelAdmin):
    list_display = ('kind', 'name', 'date', 'location', 'results')
    list_filter = ('kind',)
    search_fields = ('name', 'location')
    form = ResultsAdminForm
    inlines = [RacersInline]
    save_on_top = True


@admin.register(RaceKindDoc)
class DocumentAdmin(admin.ModelAdmin):
    list_display = ('kind', 'name', 'file')
    list_filter = ('kind',)
    search_fields = ('name',)

