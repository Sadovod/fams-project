from django.views.generic import ListView
from .models import Race, RaceKind
from ..accounts.models import CustomUser
from django.contrib import messages
from django.db import transaction
from django.shortcuts import render, redirect, reverse


class RaceKindsList(ListView):
    model = RaceKind
    template_name = 'race_kinds.html'
    context_object_name = 'race_kinds'
    paginate_by = 20


class RaceList(ListView):
    model = Race
    template_name = 'races.html'
    context_object_name = 'races'
    paginate_by = 20
    
    def get_queryset(self):
        name = self.request.GET.get('name')
        kind = self.request.GET.get('kind')
        object_list = Race.objects.all()
        if name:
            object_list = object_list.filter(name__icontains=name)
        if kind:
            object_list = object_list.filter(kind__name=kind)
        return object_list

    def get_context_data(self, *args, **kwargs):
        context = super(RaceList, self).get_context_data(*args, **kwargs)
        if self.request.user.is_authenticated:
            context['user_races'] = self.request.user.races.all()
        context['all_kinds'] = RaceKind.objects.all()
        context['kind'] = self.request.GET.get('kind', '')
        context['name'] = self.request.GET.get('name', '')
        return context


# Регистрация на гонку
@transaction.atomic()
def race_registration(request, pk):
    if request.user.is_authenticated and request.user.is_verified:
        username = request.user.username
        this_race = Race.objects.get(pk=pk)
        user = CustomUser.objects.get(username=username)
        user.races.add(this_race)
        messages.success(request, 'Вы зарегестрированы на гонку')
        return redirect(reverse('races'))
    else:
        messages.error(request, 'Для участия в гонке требуется авторизироваться на сайте')
        return redirect(reverse('races'))


# Отмена регистрации на гонку
@transaction.atomic()
def race_deregistration(request, pk):
    if request.user.is_authenticated and request.user.is_verified:
        username = request.user.username
        user = CustomUser.objects.get(username=username)
        this_race = Race.objects.get(pk=pk)
        user.races.remove(this_race)
        messages.success(request, 'Регистрация отменена')
        return redirect(reverse('races'))

    else:
        messages.error(request, 'Требуется авторизация')
        return redirect(reverse('races'))


