from django.urls import path
from .views import RaceList, RaceKindsList, race_registration, race_deregistration


urlpatterns = [
    path('', RaceKindsList.as_view(), name='race_kinds'),
    path('registration/<int:pk>', race_registration, name='race_registration'),
    path('deregistration/<int:pk>', race_deregistration, name='race_deregistration'),
]
