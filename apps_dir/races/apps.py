from django.apps import AppConfig


class RacesConfig(AppConfig):
    name = 'apps_dir.races'
    verbose_name = 'Гонки'
