from django.db import models
from django.urls import reverse


# Типы гонок
class RaceKind(models.Model):
    name = models.CharField(max_length=30, unique=True, verbose_name='Название')
    photo = models.ImageField(upload_to='races/race_kinds', verbose_name='Фотография', null=True, blank=True)

    def get_absolute_url(self):
        return reverse('races', args=[str(self.pk)])

    def get_races(self):
        return self.race_set.all()

    def __str__(self):
        return self.name

    class Meta:
        ordering = ['name']
        verbose_name = 'Вид гонок'
        verbose_name_plural = 'Виды гонок'


# Модель гонок
class Race(models.Model):
    kind = models.ForeignKey(RaceKind, null=True, on_delete=models.CASCADE, verbose_name='Вид гонки')
    date = models.DateField(verbose_name='Дата')
    name = models.CharField(max_length=50, verbose_name='Название')
    location = models.CharField(max_length=50, verbose_name='Место')
    results = models.TextField(blank=True, verbose_name='Результаты')

    def __str__(self):
        return self.name

    class Meta:
        ordering = ['-date']
        verbose_name = 'Гонка'
        verbose_name_plural = 'Гонки'


# Документы типа гонок
class RaceKindDoc(models.Model):
    kind = models.ForeignKey(RaceKind, on_delete=models.CASCADE, null=True, verbose_name='Вид гонок')
    name = models.CharField(max_length=50, verbose_name='Название')
    file = models.FileField(upload_to='races/documents', verbose_name='Файл документа')

    def __str__(self):
        return self.name

    class Meta:
        ordering = ['name']
        verbose_name = 'Регламентирующий документ'
        verbose_name_plural = 'Регламентирующие документы'

