from django.db import models


class Photo(models.Model):
    image = models.ImageField(upload_to='gallery', verbose_name='Фотография')
    pub_date = models.DateTimeField(auto_now=True, verbose_name='Дата загрузки')

    class Meta:
        ordering = ['-pub_date']
        verbose_name = 'Фотография'
        verbose_name_plural = 'Фотографии'
