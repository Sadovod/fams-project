from django.apps import AppConfig


class GalleryConfig(AppConfig):
    name = 'apps_dir.gallery'
    verbose_name = 'Галерея'
