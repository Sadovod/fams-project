from django.views.generic import ListView
from .models import Photo


class PhotoList(ListView):
    model = Photo
    template_name = 'gallery.html'
    paginate_by = 25
    context_object_name = 'photos'
