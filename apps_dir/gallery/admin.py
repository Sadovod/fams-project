from django.contrib import admin
from .models import Photo


@admin.register(Photo)
class PhotoAdmin(admin.ModelAdmin):
    list_display = ('pub_date',)
    readonly_fields = ('pub_date',)
    search_fields = ('pub_date',)

