from django.shortcuts import render
from .models import Judge, Member, Partner, DocKind, Sponsor, Document
from ..races.models import RaceKind
from ..news.models import News


def home_view(request):
    latest_news = News.objects.all()[:3]
    race_kinds = RaceKind.objects.all()
    sponsors = Sponsor.objects.all().order_by('?')
    partners = Partner.objects.all().order_by('?')
    context = {
        'latest_news': latest_news,
        'race_kinds': race_kinds,
        'sponsors': sponsors,
        'partners': partners,
    }
    return render(request, 'home.html', context=context)


def federation_view(request):
    members = Member.objects.all()
    documents = Document.objects.all()
    doc_kinds = DocKind.objects.all()
    judges = Judge.objects.all()

    context = {
        'members': members,
        'doc_kinds': doc_kinds,
        'documents': documents,
        'judges': judges,
    }

    return render(request, 'federation.html', context=context)


# def documents_view(request):
#     document_kinds = DocKind.objects.all()
#     context = {'doc_kinds': document_kinds}
#     return render(request, 'documents.html', context=context)


def sponsors_view(request):
    sponsors = Sponsor.objects.all().order_by('?')
    context = {'sponsors': sponsors}
    return render(request, 'sponsors.html', context=context)


def partners_view(request):
    partners = Partner.objects.all().order_by('?')
    context = {'partners': partners}
    return render(request, 'partners.html', context=context)
