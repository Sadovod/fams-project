from django.urls import path
from .views import home_view, sponsors_view, partners_view, federation_view


urlpatterns = [
    path('sponsors/', sponsors_view, name='sponsors'),
    path('partners/', partners_view, name='partners'),
    path('federation/', federation_view, name='federation'),
    path('', home_view, name='home')
]
