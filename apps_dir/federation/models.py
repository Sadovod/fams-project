from django.db import models
from django.urls import reverse
from ..accounts.models import CustomUser


class Judge(models.Model):
    name = models.CharField(max_length=100, verbose_name='ФИО')
    photo = models.ImageField(upload_to='federation/judges', verbose_name='Фотография', null=True, blank=True)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ['name']
        verbose_name = 'Судья'
        verbose_name_plural = 'Судьи'


class Message(models.Model):
    user = models.ForeignKey(CustomUser, on_delete=models.SET_NULL, null=True, verbose_name='Пользователь')
    text = models.TextField(verbose_name='Сообщение')

    class Meta:
        verbose_name = 'Сообщение'
        verbose_name_plural = 'Сообщения'


class Member(models.Model):
    name = models.CharField(max_length=60, verbose_name='ФИО')
    photo = models.ImageField(upload_to='federation/members', verbose_name='Фото')
    position = models.CharField(max_length=100, verbose_name='Должность')

    class Meta:
        verbose_name = 'Сотрудник федерации'
        verbose_name_plural = 'Сотрудники федерации'


class Sponsor(models.Model):
    name = models.CharField(max_length=30, verbose_name='Название')
    logo = models.ImageField(upload_to='federation/sponsors', verbose_name='Логотип')
    url = models.URLField(verbose_name='URL сайта')

    class Meta:
        verbose_name = 'Спонсор'
        verbose_name_plural = 'Спонсоры'


class Partner(models.Model):
    name = models.CharField(max_length=30, verbose_name='Название')
    logo = models.ImageField(upload_to='federation/partners', verbose_name='Логотип')
    url = models.URLField(verbose_name='URL сайта')

    class Meta:
        verbose_name = 'Партнер'
        verbose_name_plural = 'Партнеры'


class DocKind(models.Model):
    name = models.CharField(max_length=50, unique=True, verbose_name='Название')

    def get_docs(self):
        return self.document_set.all()

    def get_absolute_url(self):
        return reverse('documents_list', args=[str(self.pk)])

    def __str__(self):
        return self.name

    class Meta:
        ordering = ['id']
        verbose_name = 'Категория документов'
        verbose_name_plural = 'Категории документов'


class Document(models.Model):
    category = models.ForeignKey(DocKind, null=True, on_delete=models.CASCADE, verbose_name='Категория')
    name = models.CharField(max_length=50, verbose_name='Название')
    file = models.FileField(upload_to='documents', verbose_name='Файл')

    def __str__(self):
        return self.name

    class Meta:
        ordering = ['id']
        verbose_name = 'Документ'
        verbose_name_plural = 'Документы'
