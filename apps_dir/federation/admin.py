from django.contrib import admin
from .models import Judge, Member, DocKind, Document, Partner, Sponsor


@admin.register(Judge)
class JudgeAdmin(admin.ModelAdmin):
    list_display = ('id', 'name',)


@admin.register(Member)
class MemberAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'photo', 'position')


@admin.register(Sponsor)
class PartnerAdmin(admin.ModelAdmin):
    list_display = ('id', 'name',)


@admin.register(Partner)
class PartnerAdmin(admin.ModelAdmin):
    list_display = ('id', 'name',)


@admin.register(DocKind)
class JudgeAdmin(admin.ModelAdmin):
    list_display = ('id', 'name',)


@admin.register(Document)
class JudgeAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'category')
    list_filter = ('category',)

