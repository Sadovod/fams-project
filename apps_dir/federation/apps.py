from django.apps import AppConfig


class FederationConfig(AppConfig):
    name = 'apps_dir.federation'
    verbose_name = 'Федерация'
