from django import forms


class MessageForm(forms.Form):
    text = forms.CharField(widget=forms.Textarea(attrs={'rows': 10, 'cols': 45, 'placeholder': 'Сообщение'}), )
