from django.apps import AppConfig


class NewsConfig(AppConfig):
    name = 'apps_dir.news'
    verbose_name = 'Новости'
