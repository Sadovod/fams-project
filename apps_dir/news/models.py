from django.db import models
from django.urls import reverse
from django.utils import timezone


class News(models.Model):
    title = models.CharField(max_length=100, verbose_name='Заголовок')
    image = models.ImageField(upload_to='news/images', verbose_name='Титульное изображение')
    text = models.TextField(verbose_name='Содержание')
    pub_date = models.DateTimeField(default=timezone.now(), verbose_name='Дата и время публикации')

    def get_absolute_url(self):
        return reverse('news_detail', args=[str(self.pk)])

    def __str__(self):
        return self.title

    class Meta:
        ordering = ['-pub_date']
        verbose_name = 'Новость'
        verbose_name_plural = 'Новости'
