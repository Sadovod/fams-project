from django.views.generic import DetailView, ListView
from .models import News
from ..races.models import Race


class NewsList(ListView):
    model = News
    template_name = 'news.html'
    paginate_by = 9
    context_object_name = 'news'

    def get_queryset(self):
        search = self.request.GET.get('q')
        if search:
            object_list = News.objects.filter(title__icontains=search)
        else:
            object_list = News.objects.all()
        return object_list

    def get_context_data(self, *args, **kwargs):
        context = super(NewsList, self).get_context_data(*args, **kwargs)
        context['q'] = self.request.GET.get('q', '')
        context['upcoming_races'] = Race.objects.all()[:5]
        return context


class NewsDetail(DetailView):
    model = News
    template_name = 'news-single.html'
    context_object_name = 'news'





